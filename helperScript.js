const ringApiSdkClass = require('./Class/RingApiSdk');
const config = require('./configFor.json');

var ringApiSdk = new ringApiSdkClass.RingApiSdk(config);



async function doSomething() {


//await makeWebsite("192a8b63-b03e-489b-8453-5f18f313fec3",'aktuality.sk');

    let websites = await ringApiSdk.websiteSearch();
    console.log(websites);
    process.exit();

    // await setNamespaceLanguage('sk');

    async function setNamespaceLanguage(languageCode) {
        let resource = await ringApiSdk.namespaceGet();
        if (resource) {
            resource.language_code = languageCode;
            let res = await ringApiSdk.namespaceSave(resource);
            console.log(res);
            process.exit();
        }


    }

    async function makeWebsite(mainCategoryUuid, name) {
        let resource = {
            "engine": {
                "endpoint": "publish.router.ucs2.onetapi.pl",
                "uuid": "eef75563-b341-4fba-82f0-535412ee667b"
            },
            "engine_data": {
                "publisher": {
                    "uuid": "2774dd36-9c14-4cf8-9e89-da3343ed8a49"
                },
                "ucs": {
                    "registered": true
                }
            },
            "match_rules": [
                {
                    "topics": [
                        {
                            "uuid": mainCategoryUuid
                        }
                    ],
                    "type": {
                        "uuid": "8e656464-30ff-46e0-aba3-a127f8dce98e"
                    }
                }
            ],
            "name": name,
            "status": "active"
        };
        let res = await ringApiSdk.websiteSave(resource);
        console.log(res);
        process.exit();
    }



    return 0;


}



doSomething().then(() => {
    console.log('we are done here');
}).catch((error) => {
    console.log(error);
    process.exit();
});

