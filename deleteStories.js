const ringApiSdkClass = require('./Class/RingApiSdk');
const config = require('./config.json');
const {UtilsTools} = require('./Class/UtilsTools');

var ringApiSdk = new ringApiSdkClass.RingApiSdk(config);


var page = 0;
const perPage = 100;
var i = 0;


async function deletePage() {
    try {
        let res = await ringApiSdk.storySearch({published: false}, null, perPage, page * perPage);

        let allCount = res.count;
        let allPages = parseInt(allCount / perPage) + 1;

        if (res.entries.length == 0) {
            console.log('All done');
            process.exit();
        }


        await UtilsTools.asyncForEach(res.entries, async item => {
            i++;
            let pubPackage = await ringApiSdk.storyGet(item.id);
            // console.log(item.id);
            // pubPackage.meta.puls.topics = [];
            pubPackage.status = 'deleted';
            let res = await ringApiSdk.storySave(pubPackage);
            console.log(`${i}/${allCount} page ${page + 1}/${allPages}`);
        });

        page++;
        deletePage();
    } catch (e) {
        console.log('error');
        console.error(e);
        page++;
        console.log(`Batch page: ${page}`);
        deletePage();
    }

}

try {
    deletePage();
} catch (e) {
    console.log('error');
    console.error(error);
    page++;
    console.log(`Batch page: ${page}`);
    deletePage();
}


