const requestRing = require('./RingRequest');
const clientClass = require('./RingClient');

const _ = require('lodash');


const API_ENDPOINTS = {
    STORY: 'story.pulscms.eu',
    COLLECTION: 'collection.pulscms.eu',
    AUTHOR: 'author.pulscms.eu',
    IMAGE: 'image.pulscms.eu',
    SOURCE: 'source.pulscms.eu',
    TAXONOMY: 'taxonomy.pulscms.eu',
    WEBSITE: 'website.pulscms.eu',
    PUBLISH: 'publish.pulscms.eu',
    EMBED: 'embed.pulscms.eu',
    NAMESPACE: 'namespace.pulscms.eu',
    BATCH_PUBLISH: 'batch-publish.pulscms.eu',
};

const OPERATIONS = {
    PUBLISH: "PUBLISH",
    WITHDRAW: "WITHDRAW"
}
const imageUploadCheckStatusProbeCount = 20;
const imageCheckStatusInterval = 2000;

exports.RingApiSdk = class RingApiSdk {

    /**
     *
     * @param {object} config
     * config in format:
     * {
    "ringApi": {
        "apiKey": {
            "key": "KEY",
            "secret": "SECRET"
        },
        "apiRequestPerMinuteLimit": 180,
        "apiRequestDelaySecondsAfterReachLimit": 5,
        "user": "USER",
        "license": "LICENCE_UUID",
    }
    }
     */
    constructor(config) {
        this.config = config;
        this.apiKey = _.get(config, 'ringApi.apiKey');
        this.apiUser = _.get(config, 'ringApi.user');
        this.ringClient = new clientClass.RingClient(config);

        this.requestCount = {};
        this.timer = {};
        this.freezed = {};
        for (let endpoint in API_ENDPOINTS) {
            this.requestCount[API_ENDPOINTS[endpoint]] = 0;
            this.timer[API_ENDPOINTS[endpoint]] = 0;
            this.freezed[API_ENDPOINTS[endpoint]] = false;
        }

        this.counerInterval = setInterval(() => {
            for (let endpoint in API_ENDPOINTS) {
                if (!this.freezed[API_ENDPOINTS[endpoint]]) {
                    this.timer[API_ENDPOINTS[endpoint]]++;
                }
            }

//            let endpoint = 'TAXONOMY';
//            console.log(this.requestCount[API_ENDPOINTS[endpoint]], this.timer[API_ENDPOINTS[endpoint]]);
        }, 1000);
        this.cacheContainer = false;
    }

    setCacheContainer(cacheContainer) {
        this.cacheContainer = cacheContainer;
    }

    /**
     *
     * @param {strong} endpoint
     * @param {string} method
     * @param {object} payload
     * @returns {Promise}
     */
    callApi(endpoint, method, payload) {
//        console.log(endpoint, method);

        return new Promise((resolve, reject) => {
//             console.log(endpoint, method, payload);
            let request = new requestRing.RingRequest(endpoint, method, payload);
            this.ringClient.request(request).then(data => {
                let timeout = 0;
                if (this.isApiOverLimit(endpoint)) {
                    timeout = (60 - this.timer[endpoint] + _.get(this.config, 'ringApi.apiRequestDelaySecondsAfterReachLimit')) * 1000;
                    this.freezed[endpoint] = true;
                    this.timer[endpoint] = 0;
                }
                setTimeout(() => {
                    this.freezed[endpoint] = false;
                    data = JSON.parse(data);
                    if (data.error) {
                        //console.error(data.error);
                        return reject(data.error);
                    }
                    return resolve(data.result);
                }, timeout)

            }).catch(err => {
                reject(err);
            });
        });

    }

    isApiOverLimit(endpoint) {
        this.requestCount[endpoint]++;
        if (this.requestCount[endpoint] > _.get(this.config, 'ringApi.apiRequestPerMinuteLimit')) {
            let timeout = (60 - this.timer[endpoint] + _.get(this.config, 'ringApi.apiRequestDelaySecondsAfterReachLimit'));
            console.log(`API limit exceeded for ${endpoint} - postponed by ${timeout}seconds`);
            this.requestCount[endpoint] = 0;
            return true;
        }
        if (this.timer[endpoint] >= 60) {
            this.timer[endpoint] = 0;
        }


        return false;
    }

    /**
     *
     * @param {object} pubPackage
     * @returns {Promise}
     */
    storySave(pubPackage) {
        return this.callApi(API_ENDPOINTS.STORY, 'save', {
            resource: pubPackage,
            user: this.apiUser,
            doc_format: 'json'
        });
    }

    /**
     *
     * @param {object} pubPackage
     * @returns {Promise}
     */
    storyGet(resourceUuid) {
        return this.callApi(API_ENDPOINTS.STORY, 'get', {
            res_uuid: resourceUuid,
            doc_format: 'json'
        });
    }

    /**
     *
     * @param {object} pubPackage
     * @returns {Promise}
     */
    storyPublish(resourceUuid) {
        return this.callApi(API_ENDPOINTS.PUBLISH, 'publish', {
            resource_id: resourceUuid,
            upn: this.apiUser
        });
    }

    /**
     *
     * @param {object} pubPackage
     * @returns {Promise}
     */
    imageGet(resourceUuid) {
        return this.callApi(API_ENDPOINTS.IMAGE, 'get', {
            res_uuid: resourceUuid,
        });
    }

    /**
     *
     * @param name
     * @returns string uuid
     */
    sourceSave(name) {
        return new Promise((resolve, reject) => {

            if (!this.cacheContainer || !this.cacheContainer.getItem('source', name)) {
                this.callApi(API_ENDPOINTS.SOURCE, 'save', {
                    resource: {
                        name: name,
                    },
                    user: this.apiUser,
                }).then(data => {
                    if (this.cacheContainer) {
                        this.cacheContainer.addItem('source', name, data.res_uuid);
                    }
                    return resolve(data.res_uuid);
                }).catch(e => {
                    if (e.code == -18) {
                        if (this.cacheContainer) {
                            this.cacheContainer.addItem('source', name, e.data.uuid);
                        }
                        return resolve(e.data.uuid);
                    }
                    return reject(e);
                });
            } else {
                return resolve(this.cacheContainer.getItem('source', name));
            }
        });
    }

    imageGetUploadedImage(requestId) {
        var that = this;
        return new Promise((resolve, reject) => {
            async function checkStatus(requestId, probe) {
                that.callApi(API_ENDPOINTS.IMAGE, 'status', {
                    request_id: requestId
                }).then(data => {
                    probe++;
                    if (_.get(data, 'status') == 'DONE') {
                        let resUuid = _.get(data, 'resource_uuid');
                        if (!resUuid) {
                            return reject('No resource_uuid of image');
                        }

                        return resolve(that.imageGet(resUuid));
                    }
                    setTimeout(() => {
                        checkStatus(requestId, probe);
                    }, imageCheckStatusInterval);

                    if (probe == imageUploadCheckStatusProbeCount) {

                        return reject('image check status: too many probes for request ' + requestId);
                    }
                }).catch(err => {
                    return reject(err);
                })
            }

            checkStatus(requestId, 0);
        })
    }

    /**
     *
     * @param {string} url
     * @param {string} title
     * @param {string} sourceUuid optional
     * @returns {string|false}
     */
    async imageUpload(url, title, sourceName) {

        let sourceUuid = undefined;
        if (sourceName) {
            sourceUuid = await this.sourceSave(sourceName);
        }

        try {
            let uploadData = await this.callApi(API_ENDPOINTS.IMAGE, 'upload', {
                attributes: {
                    meta: {
                        dc: {
                            source: {
                                uuid: sourceUuid
                            },
                            title: {
                                main: title
                            }
                        },
                        puls: {
                            license: {
                                "uuid": _.get(this.config, 'ringApi.license')
                            }
                        },
                        misc: {
                            origin: {
                                repository: 'TEST_REPO',
                                id: `image_manual_url_${url}`
                            }
                        }
                    },
                    name: title
                },
                image_url: url,
                user: this.apiUser
            });

            let requestId = _.get(uploadData, 'request_id');
            if (!requestId) {
                return false;
            }
            try {
                let image = await this.imageGetUploadedImage(requestId);
                return image;
            } catch (err) {
                console.error(err);
                return false;
            }
        } catch (err) {
            if (err.code == -18 && err.data.uuid) {
                return await this.imageGet(err.data.uuid);
            } else {
                console.error(err);
                return false;
            }
        }
    }

    /**
     *
     * @param {object} pubPackage
     * @returns {Promise}
     */
    storyListKinds() {
        return this.callApi(API_ENDPOINTS.STORY, 'list_kinds', {});
    }

    /**
     *
     * @param categoryUuids
     * @returns {Promise}
     */
    websiteMatchByCategories(categoryUuids) {
        return this.callApi(API_ENDPOINTS.WEBSITE, 'match_by_category', {
            categories: categoryUuids
        });
    }

    /**
     *
     * @param resource
     * @returns {Promise} uuid
     */
    taxonomySave(resource) {
        return new Promise((resolve, reject) => {
            this.callApi(API_ENDPOINTS.TAXONOMY, 'save', {
                user: this.apiUser,
                resource: resource,
                doc_format: 'json'
            }).then(data => {
                resolve(data.res_uuid)
            }).catch(e => {
                if (e.code === -18) {
                    resolve(e.data.uuid);
                }
                reject(e);
            })
        })

    }

    /**
     *
     * @param uuid
     * @returns {Promise} uuid
     */
    taxonomyPublish(uuid) {
        return this.callApi(API_ENDPOINTS.TAXONOMY, 'publish', {
            user: this.apiUser,
            resource_id: uuid
        });

    }

    /**
     *
     * @param filters
     * @param query
     * @param limit
     * @param sort
     * @returns {Promise}
     */
    taxonomySearch(filters, query = null, limit = 10, offset = 0, sort = "score desc") {
        return this.callApi(API_ENDPOINTS.TAXONOMY, 'search', {
            filters: filters,
            limit: limit,
            query: query,
            sort: sort,
            offset: offset
        });
    }

    /**
     *
     * @param filters
     * @param query
     * @param limit
     * @param sort
     * @returns {Promise}
     */
    sourceSearch(filters, query = null, limit = 10, offset = 0, sort = "score desc") {
        return this.callApi(API_ENDPOINTS.SOURCE, 'search', {
            filters: filters,
            limit: limit,
            query: query,
            sort: sort,
            offset: offset
        });
    }

    /**
     *
     * @param filters
     * @param query
     * @param limit
     * @param sort
     * @returns {Promise}
     */
    authorSearch(filters, query = null, limit = 10, offset = 0, sort = "score desc",) {
        return this.callApi(API_ENDPOINTS.AUTHOR, 'search', {
            filters: filters,
            limit: limit,
            query: query,
            sort: sort,
            offset: offset
        });
    }

    /**
     *
     * @param filters
     * @param query
     * @param limit
     * @param sort
     * @returns {Promise}
     */
    storySearch(filters, query = null, limit = 10, offset = 0, sort = "score desc") {
        return this.callApi(API_ENDPOINTS.STORY, 'search', {
            filters: filters,
            limit: limit,
            query: query,
            sort: sort,
            offset: offset
        });
    }

    /**
     *
     * @param TEST_REPO_id
     * @returns {Promise}
     */
    findAuthorByOriginId(TEST_REPO_id) {
        return new Promise((resolve, reject) => {
            this.authorSearch({
                origin: {
                    "repository": "TEST_REPO",
                    "ids": ['author_' + TEST_REPO_id]
                }
            }).then(data => {
                let author = _.get(data, 'entries.0');
                if (author) {
                    resolve(author);
                } else {
                    resolve(false);
//                    reject('no author found ' + TEST_REPO_id);
                }
            }).catch(e => {
                reject(e);
            });
        });
    }


    /**
     *
     * @param repository
     * @param originId
     * @returns {Promise}
     */
    findAuthorByOrigin(repository, originId) {
        return new Promise((resolve, reject) => {
            return this.callApi(API_ENDPOINTS.AUTHOR, 'search', {
                filters: {
                    origin: {
                        "repository": repository,
                        "ids": [originId]
                    }
                }
            }).then(data => {
                // process.exit();
                let uuid = _.get(data, 'entries.0.id');
                if (uuid) {
                    resolve(uuid);
                } else {
                    resolve(false);
                }
            }).catch(e => {
                reject(e);
            });
        });
    }

    /**
     *
     * @param resource
     * @returns {Promise}
     */
    async authorSave(resource) {
        return await this.callApi(API_ENDPOINTS.AUTHOR, 'save', {
            user: this.apiUser,
            resource: resource,
            doc_format: 'json'
        });
    }

    /**
     *
     * @param res_uuid
     * @returns {Promise}
     */
    async authorGet(res_uuid) {
        return await this.callApi(API_ENDPOINTS.AUTHOR, 'get', {
            res_uuid: res_uuid,
            doc_format: 'json'
        });
    }

    /**
     *
     * @param categoryResource
     * @returns {Promise}
     */
    authorPublish(uuid) {
        return this.callApi(API_ENDPOINTS.AUTHOR, 'publish', {
            user: this.apiUser,
            resource_id: uuid,
        });
    }

    /**
     *
     * @param categoryResource
     * @returns {Promise}
     */
    authorWithdraw(uuid) {
        return this.callApi(API_ENDPOINTS.AUTHOR, 'withdraw', {
            user: this.apiUser,
            resource_id: uuid,
        });
    }

    /**
     * @param categoryResource
     * @returns {Promise}
     */
    taxonomyList() {
        return this.callApi(API_ENDPOINTS.TAXONOMY, 'list_taxonomies', {});
    }

    /**
     *
     * @param resource
     * @returns {Promise}
     */
    flagSave(code_name, display_name) {
        return this.callApi(API_ENDPOINTS.STORY, 'save_flag', {
            code_name: code_name,
            display_name: display_name
        });
    }

    flagList() {
        return this.callApi(API_ENDPOINTS.STORY, 'list_flags', {});
    }

    embedCheck(url) {
        return this.callApi(API_ENDPOINTS.EMBED, 'check', {
            url: url
        });
    }

    embedSave(resource) {
        return this.callApi(API_ENDPOINTS.EMBED, 'save', {
            resource: resource,
            user: this.apiUser
        });
    }

    embedGet(embedId) {
        return this.callApi(API_ENDPOINTS.EMBED, 'get', {
            embed_id: embedId,
        });
    }

    taxonomyListHierarchyDescendants(categoryUuid) {
        return this.callApi(API_ENDPOINTS.TAXONOMY, 'list_hierarchy_descendants', {res_uuid: categoryUuid});
    }

    /**
     *
     * @param resource
     * @returns {Promise}
     */
    websiteSave(resource) {
        return this.callApi(API_ENDPOINTS.WEBSITE, 'save', {
            resource: resource,
            user: this.apiUser
        });
    }

    /**
     *
     * @param filters
     * @param query
     * @param limit
     * @param sort
     * @returns {Promise}
     */
    websiteSearch(filters, query = null, limit = 10, sort = "score desc", offset = 0) {
        return this.callApi(API_ENDPOINTS.WEBSITE, 'search', {
            filters: filters,
            limit: limit,
            query: query,
            sort: sort,
            offset: offset
        });
    }

    /**
     *
     * @returns {Promise}
     */
    namespaceGet() {
        return this.callApi(API_ENDPOINTS.NAMESPACE, 'get', {});
    }

    /**
     *
     * @returns {Promise}
     */
    namespaceSave(resource) {
        return this.callApi(API_ENDPOINTS.NAMESPACE, 'save', {
            resource: resource,
            user: this.apiUser
        });
    }

    /**
     *
     * @param operation Operation to perform, one of PUBLISH/WITHDRAW
     * @return {Promise}
     */
    batchCreateJob(name, operation = OPERATIONS.PUBLISH) {
        return new Promise((resolve, reject) => {
            this.callApi(API_ENDPOINTS.BATCH_PUBLISH, 'createJob', {
                operation: operation,
                upn: this.apiUser,
                name: name
            })
                .then(res => {
                    resolve(res.job_id)
                })
                .catch(err => {
                    reject(err);
                })
        });

    }

    /**
     *
     * @param jobId
     * @param [] resourceIds
     * @return {Promise}
     */
    batchFillJob(jobId, resourceIds) {
        return this.callApi(API_ENDPOINTS.BATCH_PUBLISH, 'fillJob', {
            job_id : jobId,
            resource_ids: resourceIds
        });
    }

    /**
     *
     * @param jobId
     * @return {Promise}
     */
    batchStart(jobId) {
        return this.callApi(API_ENDPOINTS.BATCH_PUBLISH, 'start', {
            job_id : jobId,
        });
    }

    /**
     *
     * @param limit
     * @param offset
     * @return {Promise}
     */
    batchListJobs(limit, offset){
        return this.callApi(API_ENDPOINTS.BATCH_PUBLISH, 'listJobs', {
            limit: limit,
            offset : offset,
        });
    }

    /**
     *
     * @param limit
     * @return {Promise}
     */
    batchCancel(jobId){
        return this.callApi(API_ENDPOINTS.BATCH_PUBLISH, 'cancel', {
            job_id: jobId,
        });
    }

};