const DLSigner = require('ring-auth-node').DLSigner;

exports.RingClient = class RingClient {
    constructor(config)
    {
        this.config = config;
        let signerConfig = {
            "service": "pulsapi",
            "accessKey": this.config.ringApi.apiKey.key,
            "secretKey": this.config.ringApi.apiKey.secret
        }
        this.signer = new DLSigner(signerConfig);
    }

    getRequestBodyJson(ringRequest)
    {
        let json = {"jsonrpc": "2.0", "id": "1", "method": ringRequest.method};
        let accessKey = this.config.ringApi.apiKey.key;
        json.params = ringRequest.params;
        json.params.api_key = accessKey;
        return json;
    }

    sign(ringRequest)
    {
        let requestBodyJson = this.getRequestBodyJson(ringRequest);
        let requestBody = JSON.stringify(requestBodyJson);
        let postRequest = {
            'method': 'POST',
            'uri': '/',
            'headers': ringRequest.headers,
            'body': Buffer.from(requestBody)
        };
        let sign = this.signer.sign(postRequest);
        ringRequest.headers['Authorization'] = sign['Authorization'];
        ringRequest.headers['X-DL-Date'] = sign['X-DL-Date'];
        return ringRequest;
    }

    request(ringRequest)
    {
        return new Promise((resolve, reject) => {

            let requestBodyJson = this.getRequestBodyJson(ringRequest);
            let requestBody = JSON.stringify(requestBodyJson);

            var request = require('request');


            var options = {
                url: 'https://' + ringRequest.host + '/',
                method: 'POST',
                headers: this.sign(ringRequest).headers,
                body: requestBody
            };

            request(options, (error, response, body) => {
                if (error) {
                    reject(error);
                } else {
                    if (response.statusCode === 200) {
                        return resolve(response.body);
                    }
                    return reject(response);
                }
            });
        });
    }
};