const md5var = require('md5');
const fs = require('fs');
const console = require('tracer').console();
const _ = require('lodash');



exports.UtilsTools = class UtilsTools {

    static timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    /**
     * @param service
     * @param idPub
     * @param name
     * @return {string}
     */
    static uuidGenerator(service, idPub, name)
    {
        let uuid4all = md5var(service + idPub + name);
        uuid4all = uuid4all.substring(0, 8) + "-" + uuid4all.substring(8, 12) + "-" + "4" + uuid4all.substring(12, 15) + "-" + "a" + uuid4all.substring(15, 18) + "-" + uuid4all.substring(19, 31);
        return uuid4all;
    }

    /**
     * 
     * @param {mixed} needle
     * @param {array} haystack
     * @returns {Boolean}
     */
    static inArray(needle, haystack) {
        var length = haystack.length;
        for (var i = 0; i < length; i++) {
            if (haystack[i] == needle) {
                return true;
            }
        }
        return false;
    }

    static saveToFile(fileName, data) {
        fs.appendFile('./logs/' + fileName, JSON.stringify(data) + ",\n", function (err) {
            console.error(err);
        });
    }

    static async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    static lodash_pushIntoObjectArray(object, path, item) {
        let array = _.get(object, path, []);
        array.push(item);
        _.set(object, path, array);
        return object;
    }
};

