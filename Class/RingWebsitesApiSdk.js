const utilsToolsRing = require('./UtilsTools').UtilsTools;
const console = require('tracer').console();
const _ = require('lodash');
const request = require("request");
const aws4 = require("aws4");


exports.RingWebsitesApiSdk = class RingWebsitesApiSdk {


    constructor(config) {
        this.config = config;
    }


    callApi(module, path, method, body) {
        // console.log(endpoint, method);

        return new Promise((resolve, reject) => {
            const credentials = {
                accessKeyId: this.config.ringWebsitesApi.publicKey,
                secretAccessKey: this.config.ringWebsitesApi.privateKey
            };


            let uri = this.config.ringWebsitesApi.versionPrefix + '/' + module + '/namespaces/' + this.config.ringWebsitesApi.namespaceId + '/' + path;
            const options = {
                service: 'execute-api',
                region: this.config.ringWebsitesApi.awsRegion,
                method: method,
                // hostname: this.config.ringWebsitesApi.endpoint,
                path: uri,
                host: this.config.ringWebsitesApi.endpoint,
                headers: {
                    'Content-Type': 'application/json',
                    // 'x-oa-debug': 1
                },
                body: JSON.stringify(body)
            };

            // console.log(options);
            // process.exit();

            aws4.sign(options, credentials);

            const params = {
                uri: `https://${this.config.ringWebsitesApi.endpoint}${uri}`,
                json: true,
                headers: options.headers,
                body: body
            };

            // console.log(params);
            // process.exit();

            request[method.toLowerCase()](params, function (error, response, body) {
                if (error) {
                    return reject(error);
                } else {
                    return resolve({body: body, responseHeaders: response.headers});
                }

            });
        });

    }


    getConfiguration() {
        return this.callApi('configuration', '', 'GET');
    }

    getStructure() {
        return this.callApi('configuration', '', 'GET');
    }


    /**
     *
     * @param parentNodeUuid
     * @param nodeName
     * @returns {Promise<uuid>}
     */
    createNode(parentNodeUuid, nodeName) {
        return new Promise((resolve, reject) => {
            this.callApi('configuration', 'nodes', 'POST', {
                parentNodeUuid: parentNodeUuid,
                nodeName: nodeName
            }).then(data => {
                let location = _.get(data, 'responseHeaders.location');
                if (!location) {
                    return reject('no location');
                }
                let uuid = _.last(location.split('/'));
                return resolve(uuid);
            })
        })

    }

    updateCategoryInNode(nodeUuid, categoryUuid) {
        return this.callApi('configuration', 'nodes/'+nodeUuid, 'PATCH',{categoryUuid: categoryUuid});
    }
};