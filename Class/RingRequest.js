exports.RingRequest = class RingRequest {
    /**
     * @param {string} host
     * @param {string} method
     * @param {object} params
     */
    constructor(host, method, params)
    {
        this.host = host;
        this.method = method;
        this.params = params || {};
        this.headers = {
            'Host': host,
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Cookie': 'acc_variant={"current":"' + host + '::PUBLIC"}'
        };
    }
};
