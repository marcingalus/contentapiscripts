const ringApiSdkClass = require('./Class/RingApiSdk');
const config = require('./config.json');
const {UtilsTools} = require('./Class/UtilsTools');
const console = require('tracer').console();

var ringApiSdk = new ringApiSdkClass.RingApiSdk(config);


//-----------------PARAMS------------------//
var page = 0; //from 0
//----------------PARAMS END----------------//


const perPage = 50;



async function cancelAllJobs() {
    try {

        let jobsListRes = await ringApiSdk.batchListJobs(perPage, page * perPage);
        console.log(jobsListRes.count);
        // process.exit();
        await UtilsTools.asyncForEach(jobsListRes.jobs, async job => {
            // console.log(job);
            // process.exit();
           // if(job.resources.total == 0){
               await ringApiSdk.batchCancel(job.job_id);
           // }

        })


        page++;
        console.log(`Batch page: ${page}`);
        await cancelAllJobs();
    } catch (e) {
        console.log('error');
        console.error(e);
        page++;
        console.log(`Batch page: ${page}`);
        cancelAllJobs();
    }

}

try {
    cancelAllJobs();
} catch (e) {
    console.log('error');
    console.error(error);
    page++;
    console.log(`Batch page: ${page}`);
    cancelAllJobs();
}

