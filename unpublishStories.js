const ringApiSdkClass = require('./Class/RingApiSdk');
const config = require('./config.json');
const {UtilsTools} = require('./Class/UtilsTools');
const console = require('tracer').console();

var ringApiSdk = new ringApiSdkClass.RingApiSdk(config);


//-----------------PARAMS------------------//
const storyFilter = {status: "active", published: true};
var page = 0; //from 0
//----------------PARAMS END----------------//

var batchLimit = 5000;
const perPage = 100;
var lastPage = 1;

async function unpublishStories() {
    if(page > lastPage){
        console.log('last page');
        process.exit();
    }
    var fullOffset = page * batchLimit;
    try {
        console.log(`fullOffset = ${fullOffset}`);

        let articles = await ringApiSdk.storySearch(storyFilter, null, 1, fullOffset);
        let pages = [];

        let limit = articles.count > batchLimit ? batchLimit : articles.count;

        console.log(`Stories found: ${articles.count}, story count ${limit}`);
        for (let i = 0; i < limit / perPage; i++) {
            pages.push(i);
        }
        var uuids = [];

        await UtilsTools.asyncForEach(pages, async storyPage => {
            // process.stdout.clearLine();
            // process.stdout.cursorTo(0);
            // process.stdout.write("page: " + storyPage);
            let articles = await ringApiSdk.storySearch(storyFilter, null, perPage, (perPage * storyPage) + fullOffset);
            await UtilsTools.asyncForEach(articles.entries, async story => {
                if (story.id) {
                    uuids.push(story.id);
                }

            });
        });


        let jobId = await ringApiSdk.batchCreateJob('infor unpublish', "WITHDRAW");
        console.log(`jobID: ${jobId}`);
        if(uuids.length > 0){
            await ringApiSdk.batchFillJob(jobId, uuids);
            let startRes = await ringApiSdk.batchStart(jobId);
            console.log(startRes);
        }


        page++;
        console.log(`Batch page: ${page}`);
        await unpublishStories();
    } catch (e) {
        console.log('error');
        console.error(e);
        page++;
        console.log(`Batch page: ${page}`);
        unpublishStories();
    }

}

try {
    unpublishStories();
} catch (e) {
    console.log('error');
    console.error(error);
    page++;
    console.log(`Batch page: ${page}`);
    unpublishStories();
}

